
'use strict';


class MessageBase {
  constructor() {
    this.msgName = this.constructor.name;
  }
  
  getMsgName() {
    return this.msgName;
  }
}

module.exports = MessageBase;
