
'use strict';

const MessageBase = require('../message-base');


class MessageRealtimeError extends MessageBase {
  constructor() {
    super();
  }
}

module.exports = MessageRealtimeError;
