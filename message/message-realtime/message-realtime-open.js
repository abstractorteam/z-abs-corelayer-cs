
'use strict';

const MessageBase = require('../message-base');


class MessageRealtimeOpen extends MessageBase {
  constructor() {
    super();
  }
}

module.exports = MessageRealtimeOpen;
