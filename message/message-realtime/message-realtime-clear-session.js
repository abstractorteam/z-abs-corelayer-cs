
'use strict';

const MessageBase = require('../message-base');


class MessageRealtimeClearSession extends MessageBase {
  constructor() {
    super();
  }
}

module.exports = MessageRealtimeClearSession;
