
'use strict';

const MessageBase = require('../message-base');


class MessageRealtimeClosed extends MessageBase {
  constructor() {
    super();
  }
}

module.exports = MessageRealtimeClosed;
