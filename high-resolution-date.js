
'use strict';


class HighResolutionDate {
  constructor(date, milliSeconds) {
    this.date = date;
    this.milliSeconds = milliSeconds;
  }
}

module.exports = HighResolutionDate;
