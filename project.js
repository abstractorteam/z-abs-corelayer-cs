
'use strict';

const GuidGenerator = require('z-abs-corelayer-cs/guid-generator');


class Project {
  constructor(source = []) {
    this.source = source;
    this.projectId = GuidGenerator.create();
    this.saved = true;
  }
  
  set(source) {
    this.source = source;
  }
  
  isSaved() {
    return this.saved;
  }
  
  setSaved() {
    this.saved = true;
  }
  
  isEmpty() {
    return 0 === this.source.length;
  }
  
  toggle(key, expanded) {
    return this.privateToggle(this.source, key, expanded);
  }
  
  privateToggle(children, key, expanded) { // MOST LIKELY THAT THE NODE IS CLOSE TO THE ROOT
    for(let i = 0; i < children.length; ++i) {
      if(children[i].key === key) {
        children[i].expanded = expanded;
        return true;
      }
    }
    for(let i = 0; i < children.length; ++i) {
      if(undefined !== children[i].children) {
        if(this.privateToggle(children[i].children, key, expanded)) {
          return true;
        }
      }
    }
    return false;
  }
  
  addFileToNode(title, dir, type, node) {
    if(undefined !== node) {
      let file = {
        key: GuidGenerator.create(),
        folder: false,
        title: title,
        data: {
          path: dir,
          type: type
        }
      };
      node.children.push(file);
      this.saved = false;
      return file;
    }
  }
  
  addFile(title, dir, type, cb) {
    return this.addFileToNode(title, dir, type, this.findFolder(this.source, dir.replace(new RegExp('[\\\\]', 'g'), '/')));
  }
  
  addFolderToNode(title, dir, types, node) {
    if(undefined !== node) {
      let folder = {
        key: GuidGenerator.create(),
        folder: true,
        title: title,
        data: {
          path: dir,
          types: types
        },
        children: []
      };
      node.children.push(folder);
      this.saved = false;
      return folder;
    }
  }
  
  addFolder(title, dir, types) {
    return this.addFolderToNode(title, dir, types, this.findFolder(this.source, dir.replace(new RegExp('[\\\\]', 'g'), '/')));
  }
  
  addRootFolder(title, types) {
    let folder = {
      key: GuidGenerator.create(),
      folder: true,
      title: title,
      data: {
        path: '.',
        types: types
      },
      children: []
    }
    this.source.push(folder);
    return folder
  }  
  
  removeNode(path, key) {
    let node = this.findNode(path);
    let foundIndex = node.children.findIndex((found) => {
      return found.key === key;
    });
    if(-1 !== foundIndex) {
      node.children.splice(foundIndex, 1);
      this.saved = false;
    }
  }
  
  getRootName() {
    // FIX to be general
    let node = this.source[0];
    return `${node.data.path}/${node.title}`;
  }
  
  getFileNames(dir) {
    let node = this.findFolder(this.source, dir);
    if(undefined !== node) {
      return node.children.map((childNode) => {
        return `${childNode.data.path}/${childNode.title}`;
      });
    }
    else {
      return [];
    }
  }
  
  findNode(file) {
    let fileData = this._pathParse(file);
    let node = this.findFolder(this.source, fileData.dir);
    if(undefined !== node) {
      for(let i = 0; i < node.children.length; ++i) {
        if(node.children[i].title === fileData.base) {
          return node.children[i];
        }
      }
      return node;
    }
  }
  
  findFolder(nodes, dir) {
    let normalizedDir = dir;
    let nodeFound = nodes.find((node) => {
      return node.folder && (normalizedDir.startsWith(`${node.data.path}/${node.title}/`) || normalizedDir === `${node.data.path}/${node.title}`);
    });
    if(undefined !== nodeFound) {
      if(`${nodeFound.data.path}/${nodeFound.title}` === normalizedDir) {
          return nodeFound;  
      }
      else {
        return this.findFolder(nodeFound.children, normalizedDir);
      }
    }
  }
  
  getAllFileChildren(node) {
    let children = [];
    this._getAllFileChildren(node, children);
    return children;
  }
  
  _getAllFileChildren(node, children) {
    node.children.forEach((child)=> {
      if(child.folder) {
        this._getAllFileChildren(child, children);
      }
      else {
        children.push(child);
      }
    });
  }
  
  renameFile(node, newTitle) {
    node.title = newTitle;
    this.saved = false;
  }
  
  renamePathRecursive(node, newTitle) {
    node.title = newTitle;
    this.saved = false;
    node.children.forEach((child) => {
      this._renamePathRecursive(child, `${node.data.path}/${newTitle}`);
    });
  }
  
  _renamePathRecursive(node, path) {
    node.data.path = path;
    if(node.folder) {
      node.children.forEach((child) => {
        this._renamePathRecursive(child, `${node.data.path}/${node.title}`);
      });
    }
  }
  
  _pathParse(file) {
    let parts = file.split('/');
    let lastPart = parts.pop();
    let lastIndex = lastPart.lastIndexOf('.');
    let dir = parts.join('/');
    if('.' === dir) {
      dir = file;
      lastPart = '';
    }
    if(-1 === lastIndex) {
      return {
        dir: dir,
        base: lastPart,
        ext: undefined,
        name: undefined
      };  
    }
    else {
      return {
        dir: dir,
        base: lastPart,
        ext: lastPart.substring(lastIndex),
        name: lastPart.substring(0, lastIndex)
      };
    }
  }
}

module.exports = Project;
